import {MDBCard, MDBCardImage, MDBCardBody, MDBCardTitle, MDBCardText, MDBRow, MDBCol} from 'mdb-react-ui-kit';
import Link from 'next/link'


export default function Product({info_product}) {
    return (
        <MDBRow className='row-cols-1 row-cols-md-2 g-4'>
            {
                info_product.map(product => (
                        <Link key={product.id} href={`/products/${product.id}`}>
                            <a>
                            <MDBCol>
                                <MDBCard>
                                    <MDBCardImage
                                        src={product.image_dir}
                                        alt={product.name}
                                        position='top'
                                    />
                                    <MDBCardBody>
                                        <MDBCardTitle>{product.name}</MDBCardTitle>
                                        <MDBCardText>
                                            {product.description}
                                        </MDBCardText>
                                    </MDBCardBody>
                                </MDBCard>
                            </MDBCol>
                            </a>
                        </Link>
                    )
                )
            }
        </MDBRow>
    )
}
