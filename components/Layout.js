import React from "react";
import Head from "next/head";
import Header from "./Header";
import Footer from "./Footer";
import {Container} from "react-bootstrap";

export default function Layout({children, title, description}) {
    return (
        <main>
            <Head>
                <title>{title ? `${title} - e-commerce` : 'e-commerce'}</title>
            </Head>
            <Header/>
            <Container>{children}</Container>
            <Footer/>
        </main>

    )
}
