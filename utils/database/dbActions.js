import fetch from 'isomorphic-fetch'
import {server} from "../../config/server-url";
import axios from "axios";

const getData = async (url) => {
    try{
        const data = await fetch(`${server}${url}`)
        const res = await data.json()
        return res
    }catch (err){
        console.log(err)
    }
}


const sendDate = async (url, data) =>{
    try {
        await axios.post(`${server}${url}`, {data})
        console.log('Data sent')

    } catch (err) {
        console.log(err)
    }
}
export {getData, sendDate}