import Layout from "../components/Layout";
import {MDBBtn, MDBBtnGroup, MDBContainer, MDBInput, MDBRadio} from "mdb-react-ui-kit";
import 'react-datepicker/dist/react-datepicker.css'
import React, {useState} from "react";
import {Form} from "react-bootstrap";
import DatePicker from "react-datepicker";
import {sendDate} from "../utils/database/dbActions";


export default function Signup() {

    const [selectedData, setSelectedData] = useState({
        firstname: "",
        lastname: "",
        email: "",
        password: "",
        confirm_password: "",
        phone: "",
        address: "",
        gender: "",
        date: new Date()
    })
    const handleSubmit = async (e) => {
        await sendDate('/users/signup',selectedData)
    }
    return (
        <Layout>
            <MDBContainer>
                <Form onSubmit={handleSubmit}>
                    <MDBInput label='First Name' id='typeText' type='text' onChange={e => setSelectedData({
                        ...selectedData, firstname: e.target.value,
                    })
                    }/>
                    <MDBInput label='Last Name' id='typeText' type='text' onChange={e => setSelectedData({
                        ...selectedData, lastname: e.target.value,
                    })}/>
                    <MDBInput label='Email' id='typeEmail' type='email' onChange={e => setSelectedData({
                        ...selectedData, email: e.target.value,
                    })}/>
                    <MDBInput label='Password' id='typePassword' type='password' onChange={e => setSelectedData({
                        ...selectedData, password: e.target.value,
                    })}/>
                    <MDBInput label='Confirm password' id='typePassword' type='password'
                              onChange={e => setSelectedData({
                                  ...selectedData, confirm_password: e.target.value,
                              })}/>
                    <MDBInput label='Phone Number' id='typeText' type='text' onChange={e => setSelectedData({
                        ...selectedData, phone: e.target.value,
                    })}/>
                    <MDBInput label='Address' id='typeText' type='text' onChange={e => setSelectedData({
                        ...selectedData, address: e.target.value,
                    })}/>
                    <MDBBtnGroup>
                        <MDBRadio btn btnColor='secondary' value='man' id='btn-radio' name='options' wrapperTag='span'
                                  label='Man'
                                  onChange={e => setSelectedData({
                                      ...selectedData, gender: e.target.value,
                                  })}/>
                        <MDBRadio
                            btn
                            btnColor='secondary'
                            value='woman'
                            id='btn-radio2'
                            name='options'
                            wrapperClass='mx-2'
                            wrapperTag='span'
                            label='Woman'
                            onChange={e => setSelectedData({
                                ...selectedData, gender: e.target.value,
                            })}
                        />
                        <MDBRadio btn btnColor='secondary' value='not_specify' id='btn-radio3' name='options'
                                  wrapperTag='span'
                                  label='Not specify' onChange={e => setSelectedData({
                            ...selectedData, gender: e.target.value,
                        })}/>
                    </MDBBtnGroup>
                    {/*<DatePicker selected={selectedData.date} onChange={e => setSelectedData({*/}
                    {/*    ...selectedData, date: e,*/}
                    {/*})}/>*/}
                    <DatePicker selected={selectedData.date} onChange={e => setSelectedData({
                        ...selectedData, date: e,
                    })}/>
                    <MDBBtn type="submit">Button</MDBBtn>
                </Form>
            </MDBContainer>
        </Layout>
    );
}

