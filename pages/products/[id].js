import React from 'react';
import {MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardImage} from 'mdb-react-ui-kit';
import Layout from "../../components/Layout";
import {getData} from "../../utils/database/dbActions";
import Error from "../404";


export default function Product(props) {
    if (!props.product.response.length) {
        return (
            <Error></Error>
        )
    }
    return (
        <Layout title={props.product.response[0].name} description={props.product.response[0].description}>
            <>
                <MDBCard className='mb-3'>
                    <MDBCardImage position='top' src={props.product.response[0].image_dir} alt='...'/>
                    <MDBCardBody>
                        <MDBCardTitle>{props.product.response[0].name}</MDBCardTitle>
                        <MDBCardText>
                            This is a wider card with supporting text below as a natural lead-in to additional content.
                            This
                            content is a little bit longer.
                        </MDBCardText>
                        <MDBCardText>
                            <small className='text-muted'>Last updated 3 mins ago</small>
                        </MDBCardText>
                    </MDBCardBody>
                </MDBCard>
            </>
        </Layout>
    )


}

export async function getServerSideProps(context) {
    const {params} = context;
    const {id} = params
    const data = await getData(`/products/one-product/${id}`)
    return {
        props: {
            product: data,
        },
    };
}