import Layout from "../components/Layout";
import Product from "../components/Product";
import {getData} from "../utils/database/dbActions";

export default function Home(props) {
    return (
        <Layout>
            <p>Hello from Index</p>
            <Product  info_product={props.products.response}/>
        </Layout>

    )
}

export async function getServerSideProps(ctx) {
    const data = await getData('/products/all-products')
    return {
        props: {
            products: data,
        },
    }
}
