import Layout from "../components/Layout";


export default function Error(props) {
    return (
        <Layout>
            <h1>404 Not Found</h1>
        </Layout>

    )
}