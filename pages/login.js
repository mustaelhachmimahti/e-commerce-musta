import Layout from "../components/Layout";
import {Form} from "react-bootstrap";
import {MDBBtn, MDBInput} from "mdb-react-ui-kit";
import React, {useState} from "react";
import axios from "axios";
import {server} from "../config/server-url";
import {sendDate} from "../utils/database/dbActions";

export default function Login() {
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    })
    const handleSubmit = async (e) => {
       await sendDate('/users/login', formData)
    }
    return (
        <Layout>
            <Form onSubmit={handleSubmit}>
                <MDBInput label='Email' type='email' onChange={e => setFormData({
                    ...formData, email: e.target.value,
                })
                }/>
                <MDBInput label='Password' type='password' onChange={e => setFormData({
                    ...formData, password: e.target.value,
                })
                }/>
                <MDBBtn>Button</MDBBtn>
            </Form>
        </Layout>
    );
}