import express from 'express';
import products from "./src/controler/products.mjs";
import users from "./src/controler/users.mjs";
import bodyParser from "body-parser";
import {config} from "dotenv";
import cors from 'cors'
import morgan from 'morgan'
import * as session from 'express-session'

config()

const app = express();
app.set('port', process.env.SERVER_PORT || 3080)

app.use(cors())
app.use(morgan('tiny'));


// app.use(session({
//     secret: 'keyboard cat',
//     resave: false,
//     saveUninitialized: true,
//     cookie: { secure: true }
// }))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/products', products)
app.use('/users', users)

app.listen(app.get('port'), () =>
    console.log('Server on port', app.get('port')),
);