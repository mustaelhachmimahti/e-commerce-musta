import express, {response} from "express";
import executeQuery from "../../database.mjs";

const router = express.Router()

router.get('/all-products', async (req, res) => {
    const allProducts = await executeQuery('SELECT * FROM products', [])
    res.send(JSON.stringify({status: 200, response: allProducts}))
})
router.get('/one-product/:id', async (req, res) => {
    const id = req.params.id;
    const oneProduct = await executeQuery('SELECT * FROM products WHERE id = ?', [id])
    res.send(JSON.stringify({status: 200, response: oneProduct}));
})


export default router;