import express from 'express';
const router = express.Router();
import bcrypt  from 'bcrypt';
import moment from 'moment';
import executeQuery from '../../database.mjs';
import nc from 'next-connect'
const handle = nc()

router.post('/login', async (req, res) => {

    const {body} = req;
    const {data} = body;
    const login = await executeQuery('SELECT * FROM users WHERE email_user = ?', [data.email])
    if(login.length){
        bcrypt.compare(data.password, login[0].password_user, function(err, response) {
            if (response) {
                console.log('OKEY')
            } else {
                console.log('NO OKEY')
            }
        });
    }else{
        console.log('not exist')
    }



});
router.post('/signup', async (req, res) => {
    const {body} = req;
    const {data} = body;
    console.log(body);
    const passwordHashed = await bcrypt.hash(data.password, 10);
    const date = moment(data.date).format('YYYY-MM-DD');
    await executeQuery(
        'INSERT INTO users( name_user, last_name_user, email_user, password_user, phone_number_user, address_user, birthday_user, gender_user) VALUES (?,?,?,?,?,?,?,?)',
        [
            data.firstname,
            data.lastname,
            data.email,
            passwordHashed,
            data.phone,
            data.address,
            date,
            data.gender,
        ]
    );
});
export default router;
