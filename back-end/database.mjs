import { createPool } from 'mysql'
const pool = createPool({
    host: 'localhost',
    user: 'soymusta',
    password: '$soymusta$',
    port: 3306,
    database: 'commerce_project',
});

pool.getConnection(err => {
    if (err) {
        console.log('Error connecting to db...');
    } else {
        console.log('Connected to db...');
    }
});

const executeQuery = (query, arraParms) => {
    return new Promise((resolve, reject) => {
        try {
            pool.query(query, arraParms, (err, data) => {
                if (err) {
                    console.log('Error executing query...');
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        } catch (err) {
            reject(err);
        }
    });
};

 export default executeQuery;